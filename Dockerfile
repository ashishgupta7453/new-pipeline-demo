FROM ubuntu as intermediate
RUN \
    apt-get -y update && \
    apt-get install -y wget && \
    apt-get install -y git && \
    mkdir -p /root/.ssh
ENV CODE_LOCATION /home/ubuntu
ENV SSH_KNOWNHOST root/.ssh/known_hosts
ENV IDRSA_PATH /root/.ssh/id_rsa
RUN echo "testing"
RUN echo "testing"
ADD id_rsa $IDRSA_PATH
RUN \
    chmod 700 $IDRSA_PATH && \
    chown -R root:root /root/.ssh && \
    touch $SSH_KNOWNHOST && \
    ssh-keyscan -H bitbucket.org >> $SSH_KNOWNHOST && \
    git clone -b feature/DEV-227 git@bitbucket.org:NumiHealth/code.git  $CODE_LOCATION
FROM ubuntu
ENV CODE_LOCATION /home/ubuntu
ENV HTML_PATH /var/www/html/
ENV APACHE2_SITESAVAILABLE /etc/apache2/sites-available/000-default.conf
COPY --from=intermediate $CODE_LOCATION $CODE_LOCATION
WORKDIR $CODE_LOCATION/numi-web
RUN \
    apt-get -y update && \
    apt-get install -y wget && \
    apt-get -y install nodejs -y && \
    apt-get -y install jq && \
    apt-get install -y npm && \
    apt-get install apache2 -y && \
    mkdir /ui_configs_web && jq -n '{"api_url":"https://uat-backend.numihealth.com/app/api"}' > /ui_configs_web/ui_conf.json && \
    npm install && \
    npm audit fix && \
    npm run build && \
    npm run export && \
    rm -r $HTML_PATH* && \
    mv $CODE_LOCATION/numi-web/out/* $HTML_PATH && \
    a2enmod rewrite && \
    sed -i '2 i <Directory /var/www/html>' $APACHE2_SITESAVAILABLE && \
    sed -i '3 i Options Indexes FollowSymLinks MultiViews' $APACHE2_SITESAVAILABLE && \
    sed -i '4 i AllowOverride All' $APACHE2_SITESAVAILABLE && \
    sed -i '5 i Require all granted' $APACHE2_SITESAVAILABLE && \
    sed -i '6 i </Directory>' $APACHE2_SITESAVAILABLE && \
    apt-get -y install nano
EXPOSE 80
CMD apachectl -D FOREGROUND
